# [Full report with graphs](https://isbjornlabs.com/stress-test/)

#  Test and Report informations

| Start Time        | End Time          |
|-------------------|-------------------|
|"10/18/18, 3:46 PM"|"10/18/18, 4:03 PM"|

# Statistics

### Executions

| #Samples | KO | Error |
|----------|----|-------|
| 2000     | 2  | 0.10% |

### Response Times (ms)

| Average  | Min  | Max    | 90th pct  | 95th pct  | 99 pct    | Throughput |
|----------|------|--------|-----------|-----------|-----------|------------|
| 82516.88 | 1188 | 622637 | 112331.00 | 137495.55 | 246914.90 | 1.88       |

### Network (KS/s)

| Received | Sent    |
|----------|---------|
| 0.26     | 5791.36 |

# Errors

| Type of error | Number of errors | % in errors | % in all samples |
| --------------|------------------|-------------|------------------|
| Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset | 2 | 100.00% | 0.10% |

