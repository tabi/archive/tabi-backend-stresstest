# tabi-backend-stresstest

Testing API with [insomnia](https://insomnia.rest/), actual stress test with [JMeter](https://jmeter.apache.org/).

## Files

### [00_insomnia_test_runs.md](./00_insomnia_test_runs.md)

Exploration of the API. Translating working commands to cURL.

### [01_JMeter_stress_test.md](./01_JMeter_stress_test.md)

Stress test with Apache JMeter.